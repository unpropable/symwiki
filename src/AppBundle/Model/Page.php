<?php

namespace AppBundle\Model;

use AppBundle\Model\om\BasePage;
use Symfony\Component\Validator\Constraints as Assert;
use \PropelPDO;
use Cocur\Slugify\Slugify;

class Page extends BasePage
{
    /**
     * @Assert\NotBlank()
     */
    protected $title;

    /**
     * @Assert\NotBlank()
     */
    protected $content;
    
    /**
     * @Assert\NotEqualTo(
     *     value = "add"
     * )
     * @Assert\NotEqualTo(
     *     value = "edit"
     * )
     * @Assert\NotEqualTo(
     *     value = "delete"
     * )
     * @Assert\NotEqualTo(
     *     value = "/"
     * )
     * @Assert\Regex("~^[a-z0-9_]*$~")
     */
    protected $alias;
    
    public function preSave(PropelPDO $con = null)
    {
        $alias = $this->getAlias();
        if(empty($alias)){
            $slug = Slugify::create('/([^A-Za-z0-9]|_)+/')->slugify($this->getTitle(),'_');
            $this->setAlias($slug);
        }
        
        if($this->hasParent()){
            $parent_path = $this->getParent()->getFullpath();            
            if($parent_path){
                $parent_path .= '/';
            }
            $this->setFullpath($parent_path.$this->getAlias());
        }

        return true;
    }  
}
