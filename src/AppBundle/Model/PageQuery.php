<?php

namespace AppBundle\Model;

use AppBundle\Model\om\BasePageQuery;

class PageQuery extends BasePageQuery
{
    public static function getNodeBySlug($slug){
        if(empty($slug)){
            $rootNode = PageQuery::create()->findRoot();
            if(empty($rootNode)){
                $rootNode = PageQuery::initRoot();
            }
            return $rootNode;
        }
        
        $page = PageQuery::create()->findOneByFullpath($slug);
        
        return $page;
    }
    
    public static function initRoot(){
        $root = new Page();
        $root->setTitle('Home');
        $root->setAlias('');
        $root->setFullpath('');
        $root->setContent('Корневая страница сайта');
        $root->makeRoot();
        $root->save(); 
        return $root;
    }
}
