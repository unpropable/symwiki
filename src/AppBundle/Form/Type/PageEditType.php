<?php

namespace AppBundle\Form\Type;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PageEditType extends BaseAbstractType
{
    protected $options = array(
        'data_class' => 'AppBundle\Model\Page',
        'name'       => 'page',
    );

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title','text', array('label'=>'Заголовок'));
        $builder->add('content','textarea', array('label'=>'Содержимое'));
    }
}
