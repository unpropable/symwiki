<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Model\Page;
use AppBundle\Model\PageQuery;
use AppBundle\Form\Type\PageType;
use AppBundle\Form\Type\PageEditType;

class DefaultController extends Controller
{
    /**
     * @Route("/add", name="add")
     * @Route("/{slug}/add", requirements={"slug":"[0-9A-z\/]+"}, name="addChild")
     */
    public function addAction($slug=null)
    {
        $parent = PageQuery::getNodeBySlug($slug);
        if(empty($parent)){
            return $this->commonRender('default/404.html.twig');
        }
        
        $page = new Page();
        $form = $this->createForm(new PageType(), $page);

        $request = $this->getRequest();
        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $page->insertAsLastChildOf($parent);
                $page->save();
                $redirect_link = $this->get('router')->generate('item', array('slug' => $page->getFullpath()));
                return $this->redirect($redirect_link);
            }
        }
        
        return $this->commonRender('default/add.html.twig', array(
            'form' => $form->createView(),
            'page' => $parent,
            'slug' => $slug));
    }
    
    /**
     * @Route("/{slug}/edit", requirements={"slug":"[0-9A-z\/]+"}, name="edit")
     */
    public function editAction($slug=null)
    {
        $page = PageQuery::getNodeBySlug($slug);
        if(empty($page)){
            return $this->commonRender('default/404.html.twig');
        }
        
        $form = $this->createForm(new PageEditType(), $page);

        $request = $this->getRequest();
        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $page->save();
                $redirect_link = $this->get('router')->generate('item', array('slug' => $page->getFullpath()));
                return $this->redirect($redirect_link);
            }
        }
        
        return $this->commonRender('default/edit.html.twig', array(
            'form' => $form->createView(),
            'page' => $page,
            'slug' => $slug));
    }
    
    /**
     * @Route("/{slug}/delete", requirements={"slug":"[0-9A-z\/]+"}, name="delete")
     */
    public function deleteAction($slug=null)
    {
        $page = PageQuery::getNodeBySlug($slug);
        if(empty($page)){
            return $this->commonRender('default/404.html.twig');
        }

        if ('POST' === $this->getRequest()->getMethod()) {
            $parent = $page->getParent();
            $page->delete();
            $redirect_link = $this->get('router')->generate('item', array('slug' => $parent->getFullpath()));
            return $this->redirect($redirect_link);
        }
        
        return $this->commonRender('default/delete.html.twig', array(
            'slug' => $slug,
            'page' => $page));
    }
    
    /**
     * @Route("/{slug}", requirements={"slug":"[0-9A-z\/]*"}, name="item")
     */
    public function indexAction($slug=null)
    {       
        $page = PageQuery::getNodeBySlug($slug);
        if(empty($page)){
            return $this->commonRender('default/404.html.twig');
        }
        
        $parent = $page->getParent();
        $childrens = $page->getChildren();
        
        $parsedContent = $this->parseContent($page->getContent());

        return $this->commonRender('default/index.html.twig', array(
            'parsedContent' => $parsedContent,
            'parent' => $parent,
            'childrens' => $childrens,
            'page' => $page));
    }
            
    private function commonRender($template, $params=array()) 
    {        
        $tree = PageQuery::create()->findRoot()->getBranch();
        foreach($tree as $node_key=>$node){
            $tree[$node_key]->current = 0;
            if(!empty($params['page']) && $node->getId() === $params['page']->getId()){
                $tree[$node_key]->current = 1;
            }
        }

        $params['tree'] = $tree;
        return $this->render($template, $params);
    }
    
    private function parseContent($content){
        $content = htmlspecialchars($content, ENT_QUOTES, 'UTF-8');
        $content = str_replace("\n", "<br>", $content);
        $content = preg_replace('/&quot;(.+?)&quot;/is', "&laquo;$1&raquo;", $content);
        $content = preg_replace("/\*\*(.+?)\*\*/is", "<b>$1</b>", $content);
        $content = preg_replace("/\/\/(.+?)\/\//is", "<i>$1</i>", $content);
        $content = preg_replace("/__(.+?)__/is", "<u>$1</u>", $content);
        
        if(preg_match_all("/\[\[(.+?)\]\]/is",$content,$matches)){
            for($i=0; $i<count($matches[1]);$i++)
            {
                $link_parts = explode(' ', $matches[1][$i]);
                $link_uri = $link_parts[0];
                $link_parts[0] = "";
                $link_desc = trim(implode(' ', $link_parts));
                if($link_desc===""){
                    $link_desc = $link_uri;
                }

                $link_uri = $this->handleUrl($link_uri);
                
                $class="";
                if($link_uri===null){
                    $class = 'class="red"';
                    $link_uri = $this->get('router')->generate('add');
                }else{
                    $link_uri = $this->get('router')->generate('item', array('slug' => $link_uri));
                }

                $link = '<a '.$class.' href="'.$link_uri.'">'.$link_desc.'</a>';
                $content = str_replace('[['.$matches[1][$i].']]',$link,$content);
            }

        }
        
        return $content;
    }
    
    private function handleUrl($string){
        if(preg_match('/(http|https):\/\/.+/is',$string)){
            return $string;
        }
        
        $page = PageQuery::create()->findOneByFullpath($string);
        if(!empty($page)){
            return $string;
        }
    }
}