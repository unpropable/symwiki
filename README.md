Simple Symfony2 wiki
========================

Requrements:
========================
* php 5.4+
* mysql

Installation
========================
* git clone https://unpropable@bitbucket.org/unpropable/symwiki.git
* cd symwiki
* curl -sS https://getcomposer.org/installer | php
* php composer.phar update
* php app/console propel:build
* php app/console propel:sql:insert --force